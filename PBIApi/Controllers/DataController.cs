﻿using System;
using System.IO;
using System.Threading;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace PBIApi.Controllers
{
    [ApiController]
    public class DataController : ControllerBase
    {
        private readonly IHostingEnvironment _environment;

        public DataController(IHostingEnvironment environment)
        {
            _environment = environment;
        }

        private bool Auth()
        {
            var auth = Request.Headers["Auth"];
            return auth.Equals("auth123");
        }

        [Route("/products")]
        public IActionResult GetProducts()
        {
            if (!Auth()) return new UnauthorizedResult();
            var products = System.IO.File.ReadAllText(Path.Join(_environment.ContentRootPath, "data", "products.json"));
            
            return new JsonResult(products);
        }
        
        
        [Route("/sales")]
        public IActionResult GetSales()
        {
            if (!Auth()) return new UnauthorizedResult();
            var sales = System.IO.File.ReadAllText(Path.Join(_environment.ContentRootPath, "data", "sales.json"));
            
            return new JsonResult(sales);
        }
        
        
        [Route("/customers")]
        public IActionResult GetCustomers()
        {
            if (!Auth()) return new UnauthorizedResult();
            var customers = System.IO.File.ReadAllText(Path.Join(_environment.ContentRootPath, "data", "customers.json"));
            return new JsonResult(customers);
        }
        
        
        [Route("/customers-to")]
        public IActionResult GetCustomersWithWait()
        {
            Thread.Sleep(1000 * 60);
            var customers = System.IO.File.ReadAllText(Path.Join(_environment.ContentRootPath, "data", "customers.json"));
            return new JsonResult(customers);
        }
        
    }
}